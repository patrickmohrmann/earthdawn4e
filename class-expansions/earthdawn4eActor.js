export default class earthdawn4eActor extends Actor {
    async getDice(step){
        console.log(`Step # is: ${step}`);
        var stepTable = ["0", "1d4-2", "1d4-1", "1d4", "1d6", "1d8", "1d10", "1d12", "2d6", "1d8+1d6", "2d8", "1d10+1d8", "2d10", "1d12+1d10", "2d12", "1d12+2d6", "1d12+1d8+1d6", "1d12+2d8", "1d12+1d10+1d8", "1d20+2d6", "1d20+1d8+1d6"];
        let dice = stepTable[step];
        return dice;
    }

    async getStep(value){
        console.log(`Value is: ${value}`);
        var step = Number([(Math.ceil(value/3))+1]);
        return step;
    }

    async explodify(dice) {
        const regex = /(d[\d]+)/g;
        const result = dice.replace(regex, `$1x`);
        console.log("Result of explodify was " + result);
        return result;
      }
}