import earthdawn4ePCsheet from "./modules/earthdawn4ePCsheet.js";
import earthdawn4eActor from "./class-expansions/earthdawn4eActor.js";


async function preloadHandlebarTemplates() {
    const templatepaths =["systems/kult4e/templates/partials/move-card.hbs", "systems/kult4e/templates/partials/relationship-card.hbs", "systems/kult4e/templates/partials/weapon-card.hbs"
    ];
    return loadTemplates(templatepaths);
};

Hooks.once("init", function() {
console.log("Initializing Earthdawn 4E");
CONFIG.Actor.entityClass = earthdawn4eActor;
Actors.unregisterSheet("core", ActorSheet);
Actors.registerSheet("earthdawn4e", earthdawn4ePCsheet, {makeDefault: true});

preloadHandlebarTemplates();
});