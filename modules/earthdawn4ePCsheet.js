export default class earthdawn4ePCsheet extends ActorSheet{
    get template(){
        return `systems/earthdawn4e/templates/sheets/pc-sheet.hbs`;
    }

    //comments2
    getData(){
        const data = super.getData();
        data.weapons = data.items.filter(function(item) {return item.type == "weapon"} );
        console.log(data);
        return data;
    }

    getData(){
      const data = super.getData();
      data.talents = data.items.filter(function(item) {return item.type == "talent"} );
      console.log(data);
      return data;
    }

      getData(){
        const data = super.getData();
        data.skills = data.items.filter(function(item) {return item.type == "skill"} );
        console.log(data);
        return data;
    
  }

    getData(){
      const data = super.getData();
      data.armor = data.items.filter(function(item) {return item.type == "armor"} );
      console.log(data);
      return data;

}


    activateListeners(html)
{
super.activateListeners(html)
    html.find('.item-delete').click(ev => {
    console.log(ev.currentTarget);
    let li = $(ev.currentTarget).parents(".item-name"),
    itemId = li.attr("data-item-id");
    console.log(li);
    console.log(`itemID =0 ${itemId}`);
    this.actor.deleteEmbeddedEntity("OwnedItem", itemId);
  });

  html.find('.item-edit').click(ev => {
    const li = $(ev.currentTarget).parents(".item-name");
    const item = this.actor.getOwnedItem(li.data("itemId"));
    item.sheet.render(true);
  });

 }}
